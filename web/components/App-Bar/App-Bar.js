/*
    LA BARRA DE APLICACIONES
    consta por ahora de un:
    - logo
    - titulo
    - boton de configuración utilizada
        para abrir el menú de configuración
*/
export const AppBar = `
    <div id="app-bar">
        <a class="Logo" href="index.html">
        </a>
        <div class="Title">
            TITTULO
        </div>
        <label class="ConfigButton" for="show-config">
        </label>
    </div>
`;