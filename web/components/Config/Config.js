/**
 * REPRESENTA EL MENÚ DE CONFIGURACIÓN
 * En este caso consta de:
 * - Un boton de cierre
 * - Un modo: light, dark
 * - Idioma
 *
 * Para poder simular los clicks del raton usamos
 * los label que tienen la peculiaridad de poder interactuar
 * con los elementos de los formularios y en este
 * caso el checkbox varia entre dos estados por lo tanto
 * con css podemos mirar esta característica para mostrar o no
 * un elemento, ya se uso en la expansión de las tarjetas
 */
export const Config =`
    <div id="config">
        <input type="checkbox" id="show-config">
        <div class="Menu">
            <div class="Row">
                <label class="ConfigButton" for="show-config">
                </label>
            </div class="Row">
            <div class="Row">
                <label class="Mode" for="config-mode">
                    <input type="checkbox" id="config-mode">
                    <div class="Toggle">
                    </div>
                    <div class="IconButton">

                    </div>
                </label>
            </div>
            <div class="Row">
                <div class="Language">
                    <input id="lang-show" type="checkbox">
                    <div class="List">
                        <input id="lang-es" type="radio" name="country" value="es" checked>
                        <label class="flag-icon flag-icon-es" for="lang-es"></label>
                        <input id="lang-gb" type="radio" name="country" value="gb" />
                        <label class="flag-icon flag-icon-gb" for="lang-gb"></label>
                    </div>
                </div>
            </div>
        </div>
    </div>
`;