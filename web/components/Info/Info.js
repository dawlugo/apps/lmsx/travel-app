/*
    REPRESENTA EL MAPA DEL SITIO Y CONTACTO

    Se usan enlaces a otras parte de tu sitio web o
    para representar las distintas redes sociales
    donde poder contactar con el soporte técnico

    Para generar los logos a veces uso svg porque se van a
    ver mejor al redimensionar y escalar pero no es
    tan bueno para manipularlos
*/
export const Info = `
    <div class="Info">
        <div class="SiteMap">
            <label>Mapa del sitio</label>
            <ul>
                <li><a href="#">Acerca de Nosotros</a></li>
                <li>
                <a href="#">Servicios</a>
                <ul>
                    <li><a href="#">Diseño Web</a></li>
                    <li><a href="#">Desarrollo de Aplicaciones</a></li>
                    <li><a href="#">Marketing Digital</a></li>
                </ul>
                </li>
                <li><a href="#">Contacto</a></li>
            </ul>
        </div>
        <div class="Social">
            <label>Redes Sociales</label>
            <ul>
                <li>
                <a href="#" class="Facebook">
                    <img
                    class="Logo"
                    src="/images/icons/facebook.svg"
                    alt="facebook logo" />
                    Facebook
                </a>
                </li>
                <li>
                <a href="#" class="Instagram">
                    <img
                    class="Logo"
                    src="/images/icons/instagram1.svg"
                    alt="instagram logo" />
                    Instagram
                </a>
                </li>
                <li>
                <a href="#" class="Youtube">
                    <img class="Logo"
                    alt="youtube logo"
                    src="/images/icons/youtube.png" />
                    Youtube
                </a>
                </li>
            </ul>
        </div>
    </div>
`;