/**
 * LA BARRA DE NAVEGACION O ROUTING
 * se usan rutas absolutas para simular
 * un sitio real así da igual donde insertamos
 * este componente dentro de la jerarquía
 * siempre irá al raíz a mirar la página principal
 */
export const NavBar=`
    <div id='nav-bar'>
        <a id="home" href="index.html">
            <div class="Text">
                Inicio
            </div>
            <div class="Icon"></div>
        </a>
        <a id="contact" href="contact.html">
            <div class="Text">
                Soporte
            </div>
            <div class="Icon"></div>
        </a>
    </div>
`;