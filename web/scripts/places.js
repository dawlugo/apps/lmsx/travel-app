/**
 * LUGARES
 * objeto Mock que representa lo guardado en las
 * tablas de una base de datos en el backend
 */
const places = new Map([
  [
    'machu-picchu', {
      id: 'machu-picchu',
      title: 'Machu Picchu',
      description: `
        Machu Picchu es una antigua ciudad inca ubicada en lo alto de los Andes,
        en la región de Cusco, Perú. Esta ciudadela se encuentra a una altitud
        de aproximadamente 2,430 metros sobre el nivel del mar y se sitúa en una
        cresta montañosa entre los picos de Machu Picchu y Huayna Picchu.

        Machu Picchu es conocida por su asombrosa arquitectura inca y su
        impresionante entorno natural. Fue construida en el siglo XV
        durante el apogeo del Imperio Inca y fue abandonada poco tiempo después,
        posiblemente debido a la conquista española.

        Esta antigua ciudad inca se ha convertido en uno de los destinos
        turísticos más populares del mundo y es considerada una de las maravillas
        arqueológicas. Machu Picchu fue declarada Patrimonio de la Humanidad
        por la UNESCO en 1983 y es una de las Siete Maravillas del Mundo Moderno.
      `,
      image: 'machu-picchu_01.avif',
      rating: 7.5,
      isFavorite: false,
      location: {
        city: 'Machu Picchu',
        region: 'Cusco',
        country: 'Perú',
      },
      reviews: [
        {
          id: 1,
          title: 'Increíble experiencia',
          content: `Machu Picchu es una maravilla que supera cualquier
            expectativa. Subir a Huayna Picchu añade emoción a la experiencia,
            ofreciendo una vista panorámica que abarca las montañas
            y las ruinas incas. La mística de este lugar te envuelve,
            transportándote a la época dorada de los incas, donde la
            arquitectura y la naturaleza coexistían en armonía`,
          rating: 8,
          author: '1',
          date: '2024-01-20',
          images: [
            '01',
            '02'
          ]
        },
        {
          id: 2,
          title: 'Un sueño',
          content: `Explorar Machu Picchu fue como adentrarse en un sueño.
            Cada rincón cuenta una historia, desde los templos hasta las
            terrazas agrícolas. La habilidad de los incas para construir
            en armonía con el entorno es evidente en cada piedra,
            y la energía que emana de este lugar es única.`,
          rating: 8.5,
          author: '2',
          date: '2024-01-21'
        },
        {
          id: 3,
          title: 'Enclave espiritual',
          content: `La Ciudadela Inca de Machu Picchu es más que un
            sitio arqueológico; es una experiencia espiritual.
            Las vistas panorámicas, las laderas verdes y la atención a los
            detalles en la construcción revelan la grandeza de una civilización
            que se fundió perfectamente con la naturaleza`,
          rating: 6.5,
          author: '3',
          date: '2024-01-22',
          images: [
            '03',
            '04',
            '05'
          ]
        },
        {
          id: 4,
          title: 'Increíble experiencia',
          content: `El lugar es asombroso. Definitivamente, volveré.`,
          rating: 8,
          author: '4',
          date: '2024-01-20'
        },
        {
          id: 5,
          title: 'El techo del mundo',
          content: `Machu Picchu es una obra maestra enclavada en los
            Andes peruanos. La serenidad que se siente al caminar entre
            las ruinas es inigualable. Es un recordatorio de la genialidad
            de los incas y la belleza eterna que puede perdurar
            a través de los siglos.`,
          rating: 7,
          author: '1',
          date: '2024-01-21',
          images: [
            '06'
          ]
        },
        {
          id: 6,
          title: 'Inolvidable',
          content: `Visitar Machu Picchu fue un viaje en el tiempo que nunca
            olvidaré. La majestuosidad de las estructuras incas, la imponente
            presencia de las montañas y el aura espiritual hacen de este lugar
            una experiencia que va más allá de la simple
            admiración arquitectónica.`,
          rating: 9,
          author: '3',
          date: '2024-01-22',
          images: [
            '07',
            '08'
          ]
        }
      ]
    }
  ],
  [
    'coliseum', {
      id: 'coliseum',
      title: 'Coliseo',
      description: `
        El Coliseo, también conocido como el Anfiteatro Flavio, es un antiguo
        anfiteatro romano situado en el centro de Roma, Italia. Es uno de los
        monumentos más emblemáticos de la antigua Roma y una de las principales
        atracciones turísticas de la ciudad.

        El Coliseo fue construido en el año 70-80 d.C. y se utilizaba para
        albergar espectáculos públicos, como batallas de gladiadores,
        representaciones teatrales, carreras de carros y eventos públicos
        en general. Tenía una capacidad para albergar a decenas de miles de
        espectadores y era un símbolo de la grandeza y el poder del
        Imperio Romano.

        A lo largo de los siglos, el Coliseo ha sufrido daños debido a
        terremotos y saqueos, pero gran parte de su estructura sigue en pie.
        En la actualidad, es un sitio arqueológico importante y un monumento
        icónico que representa la historia y la arquitectura romana.
        El Coliseo también ha sido designado como Patrimonio de la Humanidad
        por la UNESCO y forma parte de las "Propiedades del Centro Histórico
        de Roma, los bienes de la Santa Sede en esa ciudad que disfrutan de
        extraterritorialidad y San Pablo Extramuros", que es una parte de la
        lista del Patrimonio de la Humanidad.
      `,
      image: 'Coliseum_01.avif',
      rating: 6.75,
      isFavorite: false,
      location: {
        city: 'Roma',
        region: 'Lazio',
        country: 'Italia',
      },
      reviews: [
        {
          id: 4,
          title: 'Ìcono de la antigua Roma',
          content: `El Coliseo, un ícono de la antigua Roma, sigue asombrando
          con su grandiosidad. Imaginar las batallas épicas y la animada
          multitud mientras recorres sus niveles es como retroceder en el
          tiempo. Cada piedra del Coliseo es un testamento a la ingeniería
          romana y a la pasión por el entretenimiento.`,
          rating: 9,
          author: '4',
          date: '2024-01-23',
          images: [
            '01',
            '02'
          ]
        },
        {
          id: 5,
          title: 'Mejor de lo esperado',
          content: `Explorar el Coliseo es adentrarse en la historia viva.
            Las gradas que alguna vez albergaron a miles de espectadores,
            los pasillos donde gladiadores valientes lucharon por la gloria
            y la magnífica arena cuentan la historia de una época donde el
            entretenimiento se mezclaba con la política y la cultura.`,
          rating: 7.5,
          author: '5',
          date: '2024-01-24'
        },
        {
          id: 6,
          title: 'Un lugar para disfrutar',
          content: `"El Coliseo es un monumento que evoca emociones intensas.
            La grandeza de su arquitectura se combina con la solemnidad de su
            historia. Cada rincón resuena con los ecos del pasado, invitándote
            a contemplar la grandeza y la tragedia que alguna vez llenaron
            este anfiteatro`,
          rating: 8,
          author: '2',
          date: '2024-01-25',
          images: [
            '03',
            '04'
          ]
        }
      ]
    }
  ],
  [
    'taj-mahal', {
      id: 'taj-mahal',
      title: 'Taj Mahal',
      description: `
        El Taj Mahal es un monumento emblemático ubicado en la ciudad de Agra,
        en el estado de Uttar Pradesh, India. Agra se encuentra en la región
        norte de la India. El Taj Mahal es un mausoleo de mármol blanco
        construido por el emperador mogol Shah Jahan en memoria de su esposa
        Mumtaz Mahal, quien falleció durante el parto.

        Este impresionante edificio fue completado en el año 1653 y es conocido
        por su arquitectura majestuosa y su belleza artística. El Taj Mahal es
        considerado uno de los más grandes logros arquitectónicos del arte
        indoislámico y es reconocido a nivel mundial como un símbolo de amor
        y dedicación. El sitio fue declarado Patrimonio de la Humanidad por
        la UNESCO en 1983 y es una de las Nuevas Siete Maravillas del Mundo.
      `,
      image: 'taj-mahal_01.avif',
      rating: 9,
      isFavorite: true,
      location: {
        city: 'Agra',
        region: 'Uttar Pradesh',
        country: 'India',
      },
      reviews: [
        {
          id: 1,
          title: 'Vistas impresionantes',
          content: `El Taj Mahal es una oda al amor y la perfección
            arquitectónica. La delicadeza de sus detalles, desde las
            incrustaciones de mármol hasta los jardines simétricos,
            crea una atmósfera de pura elegancia. La historia de amor
            detrás de este monumento eleva su significado a una
            experiencia trascendental`,
          rating: 10,
          author: '7',
          date: '2024-01-26',
          images: [
            '01',
            '02'
          ]
        },
        {
          id: 2,
          title: 'Experiencia única',
          content: `Explorar el Taj Mahal es sumergirse en un cuento de hadas
            arquitectónico. Los detalles meticulosos de las esculturas,
            la imponente cúpula y la belleza de los jardines transmiten
            la magnificencia de la visión de Shah Jahan. Este monumento va más
            allá de la arquitectura; es una manifestación tangible
            del amor eterno.`,
          rating: 9,
          author: '8',
          date: '2024-01-27',
          images: [
            '03',
            '04'
          ]
        },
        {
          id: 3,
          title: 'Recomendado para fotógrafos',
          content: `El Taj Mahal es un espectáculo celestial en mármol blanco.
            La simetría impecable, los intrincados detalles y el reflejo
            en el agua crean una escena que parece salida de un sueño.
            Cada rincón revela la dedicación y la artesanía que se invirtieron
            en esta maravilla arquitectónica.`,
          rating: 8,
          author: '1',
          date: '2024-01-28'
        },
        {
          id: 4,
          title: 'Viaje transcendental',
          content: `Visitar el Taj Mahal es sumergirse en la grandeza de la
            historia india. La estructura, con su elegancia atemporal,
            trasciende las épocas. Cada paso dentro de sus puertas es un
            recordatorio de la belleza que puede surgir de la devoción
            y la habilidad artística.`,
          rating: 7,
          author: '2',
          date: '2024-01-28'
        },
        {
          id: 6,
          title: 'Arquitectura celestial',
          content: `El Taj Mahal es más que un monumento; es una expresión
            de amor eterno. La historia detrás de su construcción y su
            arquitectura celestial hacen de este lugar algo
            verdaderamente único.`,
          rating: 5,
          author: '6',
          date: '2024-01-28'
          ,
          images: [
            '05',
            '06',
            '07',
            '08'
          ]
        },
        {
          id: 7,
          title: 'Recomendado para fotógrafos',
          content: `Cada rincón es perfecto para capturar fotos asombrosas.`,
          rating: 8,
          author: '7',
          date: '2024-01-28'
        }

      ]
    }
  ],
  ['pyramids', {
    id: 'pyramids',
    title: 'Las pirámides',
    description: `
      Las pirámides de Egipto se encuentran en la meseta de Giza, cerca de
      El Cairo, la capital de Egipto. Esta área es conocida como la Necrópolis
      de Giza y es hogar de las tres pirámides más famosas de Egipto:
      la Gran Pirámide de Giza (también conocida como la Pirámide de Keops),
      la Pirámide de Kefrén y la Pirámide de Micerino. Estas pirámides fueron
      construidas durante el período del Antiguo Reino de Egipto y son
      monumentos emblemáticos de la civilización egipcia antigua.
    `,
    image: 'pyramids_01.avif',
    rating: 7.5,
    isFavorite: true,
    location: {
      city: 'Giza',
      region: 'El Cairo',
      country: 'Egipto',
    },
    reviews: [
      {
        id: 17,
        title: 'Enclave de ensueño',
        content: `Las Pirámides son testigos de una civilización avanzada.
          La precisión de su construcción y la majestuosidad de sus formas
          son fascinantes. Un lugar que te transporta a otra época.`,
        rating: 9,
        author: '9',
        date: '2024-02-05',
        images: [
          '01',
          '02'
        ]
      },
      {
        id: 18,
        title: 'Maravillosas',
        content: `Las Pirámides de Egipto son un monumento a la ingeniería
          y la creatividad humanas. La vista desde la meseta es impresionante
          y te hace reflexionar sobre el ingenio de la civilización antigua`,
        rating: 8.2,
        author: '8',
        date: '2024-02-06',
        images: [
          '03',
          '04'
        ]
      },
      {
        id: 19,
        title: 'Viaje en el tiempo',
        content: `Explorar las Pirámides es como hacer un viaje en el tiempo.
          La arquitectura y la ingeniería de los antiguos egipcios son
          verdaderamente sorprendentes. Un lugar lleno de misterio e historia. `,
        rating: 7.9,
        author: '7',
        date: '2024-02-07',
      }
    ]
  }],
  ['petra', {
    id: 'petra',
    title: 'Petra',
    description: `
      Petra es una antigua ciudad ubicada en la región desértica de Wadi Musa,
      en el sur de Jordania. Esta ciudad fue la capital del Reino Nabateo y es
      conocida por su arquitectura única tallada en roca, así como por sus
      elaborados sistemas hidráulicos.

      Algunos de los edificios más famosos de Petra incluyen el Tesoro
      (Al-Khazneh), el Monasterio (Ad-Deir), y el Siq, un estrecho desfiladero
      que conduce a la ciudad. El Tesoro, con su fachada esculpida y detallada,
      es uno de los monumentos más icónicos y reconocidos de Petra.

      Petra fue una ciudad próspera en la antigüedad, un importante centro de
      comercio y una conexión clave en las rutas comerciales de caravanas de
      la región. Aunque la ciudad fue abandonada y quedó en gran parte olvidada
      durante siglos, fue redescubierta por exploradores occidentales en el
      siglo XIX y desde entonces ha atraído la atención y el asombro de
      visitantes de todo el mundo.

      Petra fue declarada Patrimonio de la Humanidad por la UNESCO en 1985 y es
      uno de los sitios arqueológicos más famosos y visitados en la actualidad.
    `,
    image: 'petra_01.avif',
    rating: 6.2,
    isFavorite: true,
    location: {
      city: 'Petra',
      region: 'Wadi Musa',
      country: 'Jordania',
    },
    reviews: [
      {
        id: 20,
        title: 'Senderismo inolvidable',
        content: `La Ciudad Rosa de Petra es una maravilla arquitectónica
          tallada en las rocas. Pasear por el Siq y descubrir el Tesoro es
          como entrar en un cuento de hadas. La historia de esta ciudad perdida
          sigue viva en cada rincón`,
        rating: 8,
        author: '6',
        date: '2024-02-08',
        images: [
          '01',
          '02'
        ]
      },
      {
        id: 21,
        title: 'Aire fresco y natural',
        content: `Petra es un tesoro tallado en la roca. El Siq te lleva
          a través de un pasaje épico antes de revelar el Tesoro, una visión
          que deja sin palabras. Un viaje mágico.`,
        rating: 7.5,
        author: '4',
        date: '2024-02-09',
        images: [
          '03',
          '04'
        ]
      }
    ]
  }],
  ['great-wall', {
    id: 'great-wall',
    title: 'La gran muralla',
    description: `
      La Gran Muralla China es una extensa fortificación construida a lo largo
      de varios siglos para proteger las fronteras norteñas de China de
      invasiones y ataques. Se extiende a lo largo de aproximadamente
      21,000 kilómetros a través de diversas regiones montañosas, colinas
      y llanuras, abarcando varias provincias y regiones autónomas en el
      norte de China.

      La construcción de la Gran Muralla comenzó en el siglo VII a.C.,
      pero la mayor parte de la muralla que conocemos hoy fue construida
      durante las dinastías Ming (1368-1644) y Qing (1644-1912).
      La muralla está compuesta por secciones interconectadas, cada una
      con sus características únicas.

      La Gran Muralla China es un importante sitio histórico y cultural,
      y es reconocida como una de las maravillas arquitectónicas del mundo.
      La sección de Badaling, cerca de Pekín, es una de las áreas más
      visitadas y restauradas de la muralla. La Gran Muralla fue declarada
      Patrimonio de la Humanidad por la UNESCO en 1987.
    `,
    image: 'great-wall_01.avif',
    rating: 7.7,
    isFavorite: true,
    location: {
      city: 'China',
      region: 'Multiples',
      country: 'China',
    },
    reviews: [
      {
        id: 23,
        title: 'Impresionante',
        content: `La Gran Muralla China es una hazaña impresionante de
          ingeniería y resistencia humana. Ser testigo de sus kilómetros
          interminables es una experiencia que te hace reflexionar sobre
          el ingenio y la determinación de las generaciones pasadas.`,
        rating: 8,
        author: '4',
        date: '2024-02-11',
        images: [
          '01',
          '02'
        ]
      },
      {
        id: 24,
        title: 'Asombrosa',
        content: `La Gran Muralla China es simplemente asombrosa.
          Me quedé sin aliento al contemplar la extensión de esta maravilla
          arquitectónica. Cada ladrillo parece contar una historia de la
          antigua China, y la sensación de estar en medio de tanta historia
          es indescriptible. `,
        rating: 8.5,
        author: '5',
        date: '2024-02-12',
      },
      {
        id: 25,
        title: 'Experiencia única',
        content: `Visitar la Gran Muralla fue una experiencia que nunca
          olvidaré. Las vistas panorámicas desde lo alto son impresionantes,
          y caminar a lo largo de sus senderos te hace apreciar la habilidad
          y dedicación que se necesitó para construir esta obra maestra.`,
        rating: 9,
        author: '2',
        date: '2024-02-13',
        images: [
          '03',
          '04',
          '05'
        ]
      },
      {
        id: 26,
        title: 'Obra titánica',
        content: `Es difícil imaginar el esfuerzo que se puso en la
          construcción de la Gran Muralla hasta que la ves en persona.
          La muralla se serpentea a través de montañas y valles, mostrando
          la ingeniería y planificación increíbles de la antigua China.`,
        rating: 7.5,
        author: '1',
        date: '2024-02-13',
        images: [
          '06',
          '07'
        ]
      },
      {
        id: 27,
        title: 'Perdida en su belleza',
        content: `Mis pasos resonaron en la Gran Muralla mientras me perdía
          en sus laberínticos caminos. Los detalles de las torres de
          vigilancia y las impresionantes vistas hicieron que cada paso
          valiera la pena. Sin duda, un lugar que todos deberían visitar
          alguna vez en la vida.`,
        rating: 8,
        author: '6',
        date: '2024-02-13',
      }
    ]
  }],
  ['chichen-itza', {
    id: 'chichen-itza',
    title: 'Chichen Itza',
    description: `
      Chichén Itzá es una antigua ciudad maya ubicada en la península de
      Yucatán, México. Es conocida por sus impresionantes estructuras
      arqueológicas, que incluyen la pirámide de Kukulcán, también conocida
      como El Castillo, que es una de las imágenes más icónicas de la
      cultura maya.

      Chichén Itzá fue declarada Patrimonio de la Humanidad por la
      UNESCO en 1988 y es una de las Nuevas Siete Maravillas del Mundo
      desde 2007. Es un importante sitio arqueológico y un destino turístico
      popular que atrae a visitantes de todo el mundo.
    `,
    image: 'chichen-itza_01.avif',
    rating: 6.9,
    isFavorite: false,
    location: {
      city: 'Chichen Itza',
      region: 'Península de Yucatán',
      country: 'México',
    },
    reviews: [
      {
        id: 26,
        title: 'Un refugio sereno',
        content: `Chichen Itza es un testimonio impresionante de la
          civilización maya. El imponente templo de Kukulcán y su serpiente
          emplumada tallada en piedra son simplemente fascinantes.
          La habilidad arquitectónica y astronómica de los antiguos mayas
          brilla con intensidad en cada rincón del sitio.`,
        rating: 8,
        author: '6',
        date: '2024-02-14',
      },
      {
        id: 27,
        title: 'Vistas panorámicas impresionantes',
        content: `Explorar Chichen Itza es como dar un paseo por la historia.
          Las pirámides, los juegos de pelota y los templos cuentan la historia
          de una civilización avanzada y su conexión con el cosmos.
          La energía espiritual que se siente aquí es única.`,
        rating: 6,
        author: '7',
        date: '2024-02-15',
        images: [
          '01',
          '02'
        ]
      },
      {
        id: 28,
        title: 'Atardeceres mágicos',
        content: `Chichen Itza te transporta a otra época. Me quedé maravillado
          por la precisión de las estructuras y la riqueza cultural que emana
          de cada piedra. Es un recordatorio de la grandeza de las
          antiguas civilizaciones.`,
        rating: 7,
        author: '3',
        date: '2024-02-16',
      }
    ]
  }],
  ['opera-sidney', {
    id: 'opera-sidney',
    title: 'La opera de sidney',
    description: `
      La Ópera de Sídney es un icónico edificio y centro de artes escénicas
      situado en el puerto de Sídney, Australia. Diseñada por el arquitecto
      danés Jørn Utzon, la Ópera de Sídney es conocida por su distintiva
      arquitectura moderna y su forma única que se asemeja a velas o conchas.

      La Ópera de Sídney fue inaugurada en 1973 y se ha convertido en un
      símbolo icónico de Australia. Es un destino turístico popular y un lugar
      emblemático que ha contribuido significativamente a la identidad
      cultural de Sídney y de Australia en general. Fue designada como
      Patrimonio de la Humanidad por la UNESCO en 2007.
    `,
    image: 'opera-sidney_01.avif',
    rating: 7.5,
    isFavorite: false,
    location: {
      city: 'Sidney',
      region: 'Nueva Gales del Sur',
      country: 'Australia',
    },
    reviews: [
      {
        id: 29,
        title: 'Luces de la ciudad',
        content: `Presenciar una actuación en la Ópera de Sídney es una
          experiencia cultural incomparable. La combinación de la acústica
          excepcional y el telón de fondo del puerto hace que cada presentación
          sea memorable. Un lugar donde el arte y la arquitectura se encuentran
          de manera extraordinaria.`,
        rating: 7,
        author: '1',
        date: '2024-02-17',
      },
      {
        id: 30,
        title: 'Hito arquitectónico',
        content: `La Ópera de Sídney ofrece una vista espectacular desde
          cualquier ángulo. Ya sea desde el puente, el agua o los jardines
          circundantes, la estructura se revela en toda su grandiosidad.
          Es un hito que define el skyline de la ciudad.`,
        rating: 9,
        author: '2',
        date: '2024-02-18',
        images: [
          '01',
          '02'
        ]
      },
      {
        id: 31,
        title: 'Arquitectura moderna',
        content: `Explorar el interior de la Ópera de Sídney es adentrarse en
          un mundo de elegancia y creatividad. Los espacios interiores, las
          escaleras dramáticas y las vistas panorámicas son tan impresionantes
          como su fachada exterior. Una experiencia arquitectónica
          y cultural única.`,
        rating: 8,
        author: '3',
        date: '2024-02-19',
        images: [
          '03',
          '04',
          '05'
        ]
      },
      {
        id: 32,
        title: 'Magia',
        content: `La Ópera de Sídney es un lugar lleno de magia, especialmente
          al atardecer, cuando la luz del sol se refleja en sus conchas blancas.
          Ya sea disfrutando de una función o simplemente paseando por sus
          alrededores, la Ópera de Sídney nunca deja de cautivar.`,
        rating: 8,
        author: '4',
        date: '2024-02-19',
      }
    ]
  }],
  ['statue-of-liberty', {
    id: 'statue-of-liberty',
    title: 'La estatua de la libertad',
    description: `
      La Estatua de la Libertad está ubicada en la Isla de la Libertad
      (Liberty Island) en el puerto de Nueva York, Estados Unidos.
      La isla se encuentra en el río Hudson, cerca de la punta sur de la
      isla de Manhattan. La Estatua de la Libertad fue un regalo del pueblo
      francés a los Estados Unidos y fue inaugurada el 28 de octubre de 1886.
      Desde entonces, se ha convertido en un símbolo icónico de la libertad y
      la democracia, y es una de las atracciones turísticas
      más famosas del mundo
    `,
    image: 'statue-of-liberty_01.avif',
    rating: 8.9,
    isFavorite: true,
    location: {
      city: 'Nueva York',
      region: 'Isla de Manhattan',
      country: 'Estados Unidos',
    },
    reviews: [
      {
        id: 32,
        title: 'Símbolo de libertad',
        content: `La Estatua de la Libertad es un símbolo icónico de la
          libertad y la democracia. Su imponente presencia en la entrada del
          puerto de Nueva York sirve como recordatorio constante de los
          ideales que defiende. Verla desde el ferry, con el horizonte de la
          ciudad de fondo, es una experiencia que inspira reflexión.`,
        rating: 9,
        author: '5',
        date: '2024-02-20',
      },
      {
        id: 33,
        title: 'Serenidad',
        content: `Explorar la isla de la Estatua de la Libertad es sumergirse
          en la historia de Estados Unidos. Desde el Museo de la Inmigración
          hasta el pedestal que alberga la Estatua, cada rincón cuenta la
          historia de aquellos que buscaron una vida mejor en estas tierras.
          Es un tributo conmovedor a la diversidad y la esperanza.`,
        rating: 8,
        author: '6',
        date: '2024-02-21',
        images: [
          '01',
          '02'
        ]
      },
      {
        id: 34,
        title: 'Espectaculares vistas',
        content: `La vista panorámica desde la corona de la Estatua de la
          Libertad es simplemente espectacular. Observar la ciudad que nunca
          duerme desde esta altura te hace apreciar la grandeza de Nueva York y
          reflexionar sobre el papel fundamental que desempeña la estatua en la
          identidad de la ciudad.`,
        rating: 10,
        author: '7',
        date: '2024-02-22',
        images: [
          '03',
          '04'
        ]
      }
    ]
  }],
  ['sistine-chapel', {
    id: 'sistine-chapel',
    title: 'La capilla sixtina',
    description: `
      La Capilla Sixtina (en italiano, Cappella Sistina) es una capilla
      ubicada en el Vaticano, en la ciudad de Roma, Italia.
      Es parte del complejo del Palacio Apostólico y ha sido famosa a lo
      largo de la historia por su impresionante arte y arquitectura,
      en particular por los frescos pintados por algunos de los artistas
      más destacados del Renacimiento, incluido Michelangelo Buonarroti.

      La Capilla Sixtina es un lugar sagrado utilizado para ceremonias
      papales y también es una atracción turística importante.
      Fue designada como Patrimonio de la Humanidad por la UNESCO en 1984.
    `,
    image: 'sistine-chapel_01.avif',
    rating: 7.9,
    isFavorite: true,
    location: {
      city: 'Ciudad del Vaticano',
      region: 'Roma',
      country: 'Estado de la Ciudad del Vaticano',
    },
    reviews: [
      {
        id: 35,
        title: 'Obra maestra',
        content: `La Capilla Sixtina es una obra maestra incomparable de la
          creatividad humana. Cada centímetro cuadrado del techo y las paredes
          está adornado con frescos que narran historias bíblicas de una manera
          que desafía la imaginación. La magnitud artística de Miguel Ángel se
          despliega en cada detalle.`,
        rating: 9,
        author: '2',
        date: '2024-02-23',
      },
      {
        id: 36,
        title: 'Noches estrelladas',
        content: `Entrar en la Capilla Sixtina es entrar en un mundo donde el
        arte y la espiritualidad se fusionan de manera única. La bóveda
        celeste, la Creación de Adán y el Juicio Final son solo algunas de las
        obras maestras que adornan este sagrado recinto. La habilidad de Miguel
        Ángel para plasmar la emoción humana es simplemente sobrecogedora.`,
        rating: 8,
        author: '3',
        date: '2024-02-24',
      },
      {
        id: 37,
        title: 'Viaje visual',
        content: `La Capilla Sixtina es un viaje visual a través del tiempo y
          la fe. Las escenas detalladas y los personajes bíblicos cobran vida
          de una manera que trasciende la pintura convencional. Es un tributo
          a la visión artística y a la destreza técnica de uno de los grandes
          maestros del Renacimiento.`,
        rating: 7.7,
        author: '4',
        date: '2024-02-25',
        images: [
          '01',
          '02',
          '03',
          '04'
        ]
      }
    ]
  }],
  ['big-ben', {
    id: 'big-ben',
    title: 'Big Ben',
    description: `
      El Big Ben es el nombre informal que se utiliza comúnmente para
      referirse a la Gran Campana (Great Bell) del reloj ubicado en el
      extremo norte del Palacio de Westminster en Londres, Inglaterra.
      Sin embargo, con el tiempo, la gente ha extendido el uso del término
      "Big Ben" para referirse no solo a la campana, sino también al famoso
      reloj y la torre que lo alberga.

      El Big Ben es un ícono importante de Londres y del Reino Unido en general.
      La Torre de Isabel, que alberga el Big Ben, forma parte del
      Palacio de Westminster, que es la sede del Parlamento británico.
    `,
    image: 'big-ben_01.avif',
    rating: 7.2,
    isFavorite: false,
    location: {
      city: 'Londres',
      region: 'Westminster',
      country: 'Inglaterra',
    },
    reviews: [
      {
        id: 35,
        title: 'El paso del tiempo',
        content: `La grandeza del Big Ben se revela al acercarse a él.
          La torre gótica y el reloj meticulosamente detallado son testigos
          silenciosos del paso del tiempo. Las vistas desde el Parlamento
          hacia el Big Ben son icónicas, especialmente al atardecer cuando
          las luces de la ciudad comienzan a brillar.`,
        rating: 8,
        author: '1',
        date: '2024-02-23',
        images: [
          '01',
          '02'
        ]
      },
      {
        id: 36,
        title: 'Símbolo británico',
        content: `El Big Ben no es solo un reloj, es un símbolo de la
          resistencia británica. A lo largo de los años, ha sido testigo de
          eventos históricos y ha permanecido como un faro de estabilidad.
          Su presencia a orillas del río Támesis es un recordatorio
          constante de la grandeza de Londres.`,
        rating: 7,
        author: '3',
        date: '2024-02-24',
        images: [
          '03'
        ]
      },
      {
        id: 37,
        title: 'Reloj',
        content: `El Big Ben es más que un monumento, es un testamento a la
          habilidad arquitectónica y la precisión mecánica. Las campanadas del
          reloj resuenan con una solemnidad que evoca un sentido de continuidad
          en medio del cambio constante. Londres no sería lo mismo sin la
          presencia imponente del Big Ben.`,
        rating: 6.5,
        author: '5',
        date: '2024-02-25',
      }
    ]
  }]

]);

function avgPlaceRating(place) {
  const reviews = place.reviews;

  // Verificar si hay reseñas para evitar división por cero
  if (reviews.length > 0) {
    // Calcular la suma de los ratings
    const sumRatings = reviews.reduce(
      (sum, review) => sum + review.rating, 0);

    // Calcular la media y devolverla
    return parseFloat((sumRatings / reviews.length).toFixed(2));

  } else {
    // No hay reseñas, devolver null o un valor predeterminado
    return null;
  }
}
places.forEach((place, id) => {
  place.rating = avgPlaceRating(place);
});
