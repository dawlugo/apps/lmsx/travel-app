/**
 * USUARIOS
 *  se generan usuarios aleatorios que se guardan en el
 * local storage del navegador para utilizar siempre los mismo
 * y tener una consistencia y cargar menos datos pues
 * luego quedan cacheados
 */
// usamos picsum que a su vez usa unplash para coger
// aleatoriamente una foto que reprsentará el avatar del
// usuario de un tamaño de 256 px
function getRandomImage() {
  const idAleatorio = Math.floor(Math.random() * 1000) + 1;
  return `https://picsum.photos/id/${idAleatorio}/256`;
}

function generateUsers(size) {
  // Lista de nombres en español
  const nombres = [
    "Juan",
    "María",
    "José",
    "Ana",
    "Pedro",
    "Laura",
    "Francisco",
    "Isabel",
    "Miguel",
    "Carmen"
  ];

  // Lista de dominios de correo electrónico reales
  const dominiosEmail = [
    "gmail.com",
    "hotmail.com",
    "yahoo.com",
    "outlook.com"
  ];

  // Mapa de usuarios (constante)
  const users = new Map();

  // generamos los usuarios
  for (let i = 1; i <= size; i++) {
    const randomName = nombres[
      Math.floor(Math.random() * nombres.length)];
    const dominioAleatorio = dominiosEmail[
      Math.floor(Math.random() * dominiosEmail.length)];
    const email = `${randomName.toLowerCase()}${i}@${dominioAleatorio}`;
    //los guardamos en el mapa
    users.set(i, {
      id: i,
      name: randomName,
      email: email,
      avatar: getRandomImage()
    });

  }
  return users;
}
// intenamos leer los usuarios del almacenamiento
var users = getJsonFromStorage('users');

// si no había ya un objeto usuarios generamos uno
if (!users){
  users = generateUsers(10);
  if(users) {
    // Guardar en localStorage
    setJsonToStorage('users', users);
  }
}