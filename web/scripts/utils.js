/*
    UTILIDADES
*/
// ALIAS SELECTORES
function $(selector) {
    return document.querySelector(selector);
}

function $All(selector) {
return document.querySelectorAll(selector);
}

// BASE DE DATOS

// Guarda el objeto en el almacenamiento
// Simula una tabla o colección
function setJsonToStorage(key, value) {
    if(value) {
        // Convertir el mapa a un objeto JavaScript
        const usersObj = {};
        value.forEach((valor, clave) => {
          usersObj[clave] = valor;
        });

        // Convertir el objeto a formato JSON
        const usersJson = JSON.stringify(usersObj);

        // Guardar en localStorage
        localStorage.setItem(key, usersJson);
      }
}

// Recupera ese objeto del almacenamiento
function getJsonFromStorage(key) {
    const json = localStorage.getItem(key);
    var map;
    if (json) {
        // Convertir el JSON a un objeto JavaScript
        const obj = JSON.parse(json);

        // Convertir el objeto a un mapa
        map = new Map(Object.entries(obj));

        // Ahora, usuariosMap contiene el mapa de usuarios almacenado previamente
    }
    return map;
}

function scrollToLeft(selector, amount) {
    var el = $(selector);
    el.scrollLeft -= amount;

}

function scrollToRight(selector, amount) {
    var el = $(selector);
    el.scrollLeft += amount;
}